<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="script.js"></script>
  <link rel="stylesheet" href="./assets/css/style.css">
  <link rel="stylesheet" href="./node_modules/font-awesome/css/font-awesome.css">
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC:300|Roboto" rel="stylesheet">
  <title>Cinéma Novius</title>
</head>

<body>

  <header>

  </header>

  <!-- J'ai divisé le site en 3 section : a, b, c  -->

  <nav class="section-a">

    <div class="menu-a">

      <div class="logo">
        <img src="./assets/img/logo.png" alt="logo">
      </div>

      <ul class="menu-a1">

        <li>
          <a href="">Cinéma 1</a>
        </li>
        <li>
          <a href="">Cinéma 2</a>
        </li>
        <li>
          <a href="">Cinéma 3</a>
        </li>

      </ul>

    </div>

    <div class="menu-b">

      <ul class="menu-b1">

        <li>
          <a href="">PROGRAMME PDF <img src="./assets/img/download.png" width=14 height=18 alt="telechargement"></a>
        </li>
        <li>
          <a href="">Nos cinémas</a>
        </li>
        <li>
          <a href="">Scolaires</a>
        </li>
        <li>
          <a href="">Contact</a>
        </li>
        <li>
          <a href="">Infos pratiques</a>
        </li>
        <button class="billet">BILLETTERIE</button>
      </ul>

    </div>

  </nav>

  <nav>

    <ul class="menu-b2">

      <li>
        <a href="#menu_programmation">PROGRAMMATION</a>
        <ul id="menu_programmation">
          <li>Cinéma 1</li>
          <li>Cinéma 2</li>
          <li>Cinéma 3</li>
        </ul>
      </li>
      <li>
        <a href="">EVENEMENT</a>
      </li>
      <li>
        <a href="">AVANT-PREMIERES</a>
      </li>
      <li>
        <a href="">RENDEZ-VOUS</a>
      </li>
      <li>
        <a href="">SORTIES NATIONALES</a>
      </li>
      <li>
        <a href="">JEUNE PUBLIC</a>
      </li>

    </ul>

  </nav>

  <main class="section-b">
    <div class="section-b-left">
      <img src="./assets/img/background.png" width="140%" alt="image de fond">
    </div>

    <div class="section-b-right">
      <div class="text-right">
        <h2>SORTIE<br>NATIONALE</h2>
        <h2>Don't worry<br>He Won't Get Far On Foot</h2>
        <h3>de Gus Van Sant <br>2018</h3>
      </div>
    </div>
  </main>

  <div class="section-c">

    <h1 class="heading">Programmatio<span style="color:red">n</span></h1>

  </div>

  <div class="container">
    <div class="btn_left"><i class="fa fa-chevron-left"></i></div>
    <div class="section-c programm">
      <div class="wrapper">
        <div class="postarea">
          <div class="posts space0" id="posts">
            <div class="postbox">

              <?php
    include_once ('programmation.php');

    $programmation = new Programmation; 
    $results = $programmation->getProgrammationDates();


    foreach($results as $dates){ 
            ?>

              <div class="program-dates">
                <?php echo $dates ?>
              </div>

              <?php } ?>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="btn_right"><i class="fa fa-chevron-right"></i></div>
    <a href="" class="calendar_week">
      <p class="calendar">CALENDRIER PAR SEMAINE</p>
      <img src="./assets/img/calendar.png" width=15 height=15 alt="calendrier">
    </a>
  </div>

</body>

</html>